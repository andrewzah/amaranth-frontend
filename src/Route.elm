module Route exposing (Route(..), fromLocation, modifyUrl, href, route)

import Html exposing (Attribute)
import Navigation exposing (Location)
import Html.Attributes as Attr exposing (class, href)
import UrlParser as Url exposing (Parser, (</>), int, oneOf, s, string)


type Route
    = Home
    | Commands
    | Guilds
    | Guild String
    | About
    | NotFound


routeToString : Route -> String
routeToString route =
    let
        pieces =
            case route of
                Home ->
                    []

                Commands ->
                    [ "commands" ]

                Guilds ->
                    [ "guilds" ]

                Guild id ->
                    [ "guild", id ]

                About ->
                    [ "about" ]

                NotFound ->
                    []
    in
        "#/" ++ String.join "/" pieces


href : Route -> Attribute msg
href route =
    Attr.href (routeToString route)


route : Parser (Route -> a) a
route =
    Url.oneOf
        [ Url.map Home (s "")
        , Url.map Commands (s "commands")
        , Url.map Guilds (s "guilds")
        , Url.map Guild (s "guild" </> string)
        , Url.map About (s "about")
        ]


fromLocation : Location -> Maybe Route
fromLocation location =
    if String.isEmpty location.hash then
        Just Home
    else
        Url.parseHash route location


modifyUrl : Route -> Cmd msg
modifyUrl =
    routeToString >> Navigation.modifyUrl
