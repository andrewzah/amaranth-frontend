module Page.Commands exposing (Model, Msg, init, update, view)

{-| List all of Amaranth's commands.
-}

import Data.Commands as Commands exposing (Commands, Command)
import Html exposing (Html, div, h1, br, input, p, text)
import Html.Attributes exposing (id, class, placeholder)
import Html.Events exposing (onInput)
import Json.Decode as Decode exposing (decodeString)
import Views.Error exposing (formatErrors)
import Util exposing ((=>), modelNameContains)
import Markdown


-- MODEL --


type alias Model =
    { value : String
    , searchInput : String
    }


init : String -> Model
init value =
    { value = value
    , searchInput = ""
    }



-- VIEW --


view : Model -> Html Msg
view model =
    unwrapResponse model


viewCommands : Commands -> String -> Html Msg
viewCommands commands searchInput =
    let
        sortedCommands =
            List.sortBy .namespace commands

        filteredCommands =
            case String.isEmpty searchInput of
                True ->
                    sortedCommands

                False ->
                    List.filter (modelNameContains searchInput) sortedCommands
    in
        div [ class "content-container" ]
            [ div [ id "commands" ]
                [ input
                    [ onInput SearchInput
                    , class "input"
                    , placeholder "Search for command"
                    ]
                    []
                , div [ class "commands" ] (List.map viewCommand filteredCommands)
                ]
            ]


viewCommand : Command -> Html Msg
viewCommand command =
    div [ class "command" ]
        [ div [ class "info" ]
            [ div [ class "namespace" ] [ text (String.toUpper command.namespace) ]
            , div [ class "name" ] [ text (String.toUpper command.name) ]
            ]
        , div [ class "description" ] [ Markdown.toHtml [] command.description ]
        ]



-- UPDATE --


type Msg
    = Receive String
    | SearchInput String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Receive msg ->
            { model | value = msg } => Cmd.none

        SearchInput str ->
            { model | searchInput = str } => Cmd.none



-- HELPERS --


unwrapResponse : Model -> Html Msg
unwrapResponse model =
    let
        _ =
            Debug.log "cmds unwrapResponse" model
    in
        case decodeString Commands.wsDecoder model.value of
            Err msg ->
                text msg

            Ok response ->
                case response of
                    Commands.ValidCommandsResponse commands ->
                        viewCommands commands model.searchInput

                    Commands.ErrorCommandsResponse errors ->
                        formatErrors errors
