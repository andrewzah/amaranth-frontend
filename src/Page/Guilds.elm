module Page.Guilds exposing (Model, Msg, init, update, view)

{-| Viewing all guilds. |
-}

import Data.Guild as Guild exposing (Guild)
import Data.Guilds as Guilds exposing (Guilds)
import Html exposing (..)
import Html.Attributes exposing (class, classList, href, id, placeholder, src, style)
import Html.Events exposing (onClick, onInput)
import Util exposing ((=>), modelNameContains)
import Navigation exposing (Location)
import Json.Decode as Decode exposing (decodeString)
import Views.Error exposing (formatErrors)
import Route exposing (Route(..))


-- viewGuild & viewRole should move to Views.Guild etc
-- MODEL --


type alias Model =
    { value : String
    , searchInput : String
    }


init : String -> Model
init value =
    { value = value
    , searchInput = ""
    }



-- VIEW --


view : Model -> Html Msg
view model =
    unwrapResponse model


viewGuilds : Guilds -> String -> Html Msg
viewGuilds guilds searchInput =
    let
        filteredGuilds =
            case String.isEmpty searchInput of
                True ->
                    guilds

                False ->
                    List.filter (modelNameContains searchInput) guilds
    in
        div [ class "content-container" ]
            [ div [ id "guilds" ]
                [ input
                    [ onInput SearchInput
                    , class "input"
                    , placeholder "Search for guild"
                    ]
                    []
                , div [ class "guilds" ] (List.map viewGuild filteredGuilds)
                ]
            ]


viewGuild : Guild -> Html Msg
viewGuild guild =
    let
        rolesLength =
            guild.roles
                |> List.length
                |> toString
                |> String.append "Roles: "
                |> text

        emojiLength =
            guild.emoji
                |> List.length
                |> toString
                |> String.append "Emoji: "
                |> text

        name =
            if (String.length guild.name) > 40 then
                (String.slice 0 39 guild.name) ++ "..."
            else
                guild.name

        region =
            String.toUpper guild.region
    in
        a
            [ --(Route.href (Route.Guild guild.id))
              class "guild"
            ]
            [ div [ class "guild-header" ]
                [ img
                    [ class "icon"
                    , class region
                    , src (Guild.iconUrl guild.id guild.icon)
                    ]
                    []
                , h2 [] [ text name ]
                ]
            , div
                [ class "guild-details" ]
                [ span [ class "roles-length" ] [ rolesLength ]
                , span [ class "emoji-length" ] [ emojiLength ]
                ]
            ]



-- UPDATE --


type Msg
    = ShowGuild String
    | SearchInput String
    | Receive String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Receive msg ->
            { model | value = msg } => Cmd.none

        SearchInput str ->
            { model | searchInput = str } => Cmd.none

        ShowGuild id ->
            model => Navigation.newUrl ("#guilds/" ++ id)



-- HELPERS --


unwrapResponse : Model -> Html Msg
unwrapResponse model =
    let
        _ =
            Debug.log "unwrapResponse" model
    in
        case decodeString Guilds.wsDecoder model.value of
            Err msg ->
                text msg

            Ok response ->
                case response of
                    Guilds.ValidGuildsResponse guilds ->
                        viewGuilds guilds model.searchInput

                    Guilds.ErrorGuildsResponse errors ->
                        formatErrors errors
