module Page.Guild exposing (Model, Msg, init, update, view, viewGuild, viewRole)

{-| Viewing one guild. |
-}

import Data.Guild as Guild exposing (Guild, ID)
import Data.Guild.Role as Role exposing (Role, hexColor)
import Html exposing (..)
import Html.Attributes exposing (attribute, class, style, classList, href, id, placeholder)
import Json.Decode as Decode exposing (decodeString)
import Views.Error exposing (formatErrors)
import Util exposing ((=>))


-- MODEL --


type alias Model =
    { value : String
    , id : String
    }


init : String -> String -> Model
init value id =
    Model value id



-- VIEW --


view : Model -> Html Msg
view model =
    unwrapResponse model.value


viewGuild : Guild -> Html Msg
viewGuild guild =
    div [ class "content-container" ]
        [ h1 [] [ text guild.name ]
        , hr [] []
        , h3 [] [ text "Roles" ]
        , div [] (List.map (\role -> viewRole role) guild.roles)
        ]


viewRole : Role -> Html Msg
viewRole role =
    div [ class "roles" ]
        [ p [ style [ ( "color", Role.hexColor role.color ) ] ]
            [ text role.name ]
        ]



-- UPDATE --


type Msg
    = Receive String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Receive msg ->
            { model | value = msg } => Cmd.none



-- HELPERS --


unwrapResponse : String -> Html Msg
unwrapResponse value =
    let
        _ =
            Debug.log "value" value
    in
        case decodeString Guild.wsDecoder value of
            Err msg ->
                text msg

            Ok response ->
                case response of
                    Guild.ValidGuildResponse guild ->
                        viewGuild guild

                    Guild.ErrorGuildResponse errors ->
                        formatErrors errors
