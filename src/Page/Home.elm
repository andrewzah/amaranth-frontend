module Page.Home exposing (Model, Msg, init, update, view)

{-| The homepage. You can get here via either the / or /#/ routes.
-}

import Html exposing (..)
import Html.Attributes exposing (attribute, class, src, classList, href, id, placeholder)
import Util exposing ((=>))


-- MODEL --


type alias Model =
    { value : String }


init : String -> Model
init value =
    { value = value }



-- VIEW --


view : Model -> Html Msg
view model =
    div [ class "content-container" ]
        [ div [ id "home" ]
            [ img [ id "brand-image", src "https://i.imgur.com/iyMDSnv.png" ] []
            , h1 [] [ text "Amaranth" ]
            , p [] [ text "A multi-purpose Discord bot" ]
            , a [ id "invite-button", href "https://discordapp.com/api/oauth2/authorize?client_id=420338377737633802&permissions=2146958583&scope=bot" ]
                [ text "INVITE" ]
            ]
        ]


type Msg
    = Receive String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Receive msg ->
            { model | value = msg } => Cmd.none
