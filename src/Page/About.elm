module Page.About exposing (Model, Msg, init, update, view)

{-| About Page
-}

import Html exposing (..)
import Html.Attributes exposing (attribute, class, classList, href, id, placeholder)
import Util exposing ((=>))


-- MODEL --


type alias Model =
    { value : String }


init : String -> Model
init value =
    { value = value }



-- VIEW --


view : Model -> Html Msg
view model =
    div [ class "content-container" ]
        [ div [ id "about" ]
            [ p [] []
            , p [ class "intro" ] [ text "Amaranth is a Discord bot written in Crystal by Andrew Zah." ]
            , p [ class "intro" ] [ text "It offers functionality for guild administration and moderation, as well as utility, music, and other general commands." ]
            , p [ class "intro" ]
                [ text "Moderation commands optionally can post to a"
                , strong [] [ text " #mod-log " ]
                , text "channel. Regardless, moderation logs always post to the guild page on this website."
                ]
            , h3 [] [ text "Source" ]
            , p [ class "source" ]
                [ text "Amaranth is fully free (as in freedom), or open source, software. Take a look or contribute on "
                , a [ href "https://github.com/azah/amaranth" ] [ text "Github" ]
                , text "."
                ]
            , p [ class "source" ]
                [ text "This website (written in Elm) is also available on "
                , a [ href "https://github.com/azah/amaranth-frontend" ] [ text "Github" ]
                , text "."
                ]
            , h3 [] [ text "Data & Privacy" ]
            , p [ class "privacy" ]
                [ text "Amaranth tracks minimal statistics, such as the number of commands Amaranth has run from a given Discord user or guild. This data will"
                , i [] [ text " only " ]
                , text "ever be used for showing various usage statistics on this website."
                ]
            , p [ class "privacy" ]
                [ text "This is optional, and a guild or user can opt out at any time by running "
                , code [] [ text "opt-out" ]
                , text ". See the commands page for more information."
                ]
            , h3 [] [ text "Questions & Concerns" ]
            , p [ class "questions" ]
                [ text "Andrew can be reached via "
                , a [ href "mailto:zah@andrewzah.com" ] [ text "email" ]
                , text " or "
                , a [ href "https://discord.gg/JBxzkEn" ] [ text "Discord" ]
                , text "."
                ]
            ]
        ]


type Msg
    = Receive String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Receive msg ->
            { model | value = msg } => Cmd.none
