module Main exposing (main)

import Data.Commands
import Data.Guild exposing (ID)
import Data.Guilds
import Html exposing (Html, li, h1, h3, hr, p, Attribute, div, text, a, header)
import Html.Attributes as Attr exposing (class)
import Html.Events exposing (Options, onWithOptions, onClick)
import Json.Decode as Json
import Navigation exposing (Location)
import Page.Home as Home
import Page.Guild as Guild
import Page.Guilds as Guilds
import Page.Commands as Commands
import Page.NotFound as NotFound
import Page.About as About
import Request.Helpers exposing (buildApiUrl, rpcUrl)
import Route exposing (Route)
import Util exposing ((=>))
import WebSocket
import Views.Page as ViewPage exposing (ActivePage)


-- MODEL --


type alias Flags =
    { initialPath : String
    , apiUrl : String
    }


type Page
    = Blank
    | NotFound
    | About About.Model
    | Commands Commands.Model
    | Guild String Guild.Model
    | Guilds Guilds.Model
    | Home Home.Model


type PageState
    = Loaded Page
    | TransitioningFrom Page


type alias Model =
    { pageState : PageState
    , value : String
    , apiUrl : String
    }


initialPage : Page
initialPage =
    Blank


init : Flags -> Location -> ( Model, Cmd Msg )
init flags location =
    setRoute (Route.fromLocation location)
        { pageState = Loaded initialPage
        , value = ""
        , apiUrl = flags.apiUrl
        }



-- VIEW --


view : Model -> Html Msg
view model =
    let
        content =
            case model.pageState of
                Loaded page ->
                    viewPage page False

                TransitioningFrom page ->
                    viewPage page True
    in
        div [ class "container" ]
            [ content ]


viewPage : Page -> Bool -> Html Msg
viewPage page isLoading =
    let
        frame =
            ViewPage.frame isLoading
    in
        case page of
            Commands subModel ->
                Commands.view subModel
                    |> frame ViewPage.Commands
                    |> Html.map CommandsMsg

            Guilds subModel ->
                Guilds.view subModel
                    |> frame ViewPage.Guilds
                    |> Html.map GuildsMsg

            Guild id subModel ->
                Guild.view subModel
                    |> frame ViewPage.Other
                    |> Html.map GuildMsg

            Home subModel ->
                Home.view subModel
                    |> frame ViewPage.Home
                    |> Html.map HomeMsg

            About subModel ->
                About.view subModel
                    |> frame ViewPage.About
                    |> Html.map AboutMsg

            Blank ->
                Html.text ""
                    |> frame ViewPage.Other

            NotFound ->
                NotFound.view
                    |> frame ViewPage.Other


onClick : Msg -> Attribute Msg
onClick message =
    onWithOptions "click" noBubble (Json.succeed message)


noBubble : Options
noBubble =
    { stopPropagation = True
    , preventDefault = True
    }



-- SUBSCRIPTIONS --


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen (rpcUrl model) Receive



-- UPDATE --


type Msg
    = SetRoute (Maybe Route)
    | Receive String
    | ShowHome
    | ShowCommands
    | ShowGuild String
    | ShowGuilds
    | ShowAbout
    | CommandsMsg Commands.Msg
    | GuildsMsg Guilds.Msg
    | GuildMsg Guild.Msg
    | AboutMsg About.Msg
    | HomeMsg Home.Msg


setRoute : Maybe Route -> Model -> ( Model, Cmd Msg )
setRoute maybeRoute model =
    case maybeRoute of
        Just Route.Home ->
            { model | pageState = Loaded (Home (Home.init model.value)) } => Cmd.none

        Just Route.About ->
            { model | pageState = Loaded (About (About.init model.value)) } => Cmd.none

        Just (Route.Guild id) ->
            { model | pageState = TransitioningFrom (Guild id (Guild.init model.value id)) }
                => WebSocket.send (rpcUrl model) (Data.Guild.wsString id)

        Just Route.Guilds ->
            { model | pageState = TransitioningFrom (Guilds (Guilds.init model.value)) }
                => WebSocket.send (rpcUrl model) Data.Guilds.wsString

        Just Route.Commands ->
            { model | pageState = TransitioningFrom (Commands (Commands.init model.value)) }
                => WebSocket.send (rpcUrl model) Data.Commands.wsString

        Just Route.NotFound ->
            { model | pageState = Loaded NotFound } => Cmd.none

        Nothing ->
            { model | pageState = Loaded NotFound } => Cmd.none


getPage : PageState -> Page
getPage pageState =
    case pageState of
        Loaded page ->
            page

        TransitioningFrom page ->
            page


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    updatePage (getPage model.pageState) msg model


updatePage : Page -> Msg -> Model -> ( Model, Cmd Msg )
updatePage page msg model =
    let
        _ =
            Debug.log "apiKey" (rpcUrl model)

        viewIfLoaded cmd =
            case model.pageState of
                Loaded page ->
                    model => cmd

                TransitioningFrom page ->
                    model => Cmd.none
    in
        case ( msg, page ) of
            ( SetRoute route, _ ) ->
                setRoute route model

            ( ShowHome, page ) ->
                model => Navigation.newUrl "#"

            ( ShowAbout, _ ) ->
                model => Navigation.newUrl "#about"

            ( ShowGuilds, _ ) ->
                viewIfLoaded (Navigation.newUrl "#guilds")

            ( ShowGuild id, status ) ->
                viewIfLoaded (Navigation.newUrl ("#guilds/" ++ id))

            ( ShowCommands, _ ) ->
                { model | pageState = Loaded page } => Navigation.newUrl "#commands"

            ( GuildsMsg subMsg, Guilds subModel ) ->
                let
                    ( newModel, cmd ) =
                        Guilds.update subMsg subModel
                in
                    { model | pageState = Loaded (Guilds newModel) } => Cmd.map GuildsMsg cmd

            ( CommandsMsg subMsg, Commands subModel ) ->
                let
                    ( newModel, cmd ) =
                        Commands.update subMsg subModel
                in
                    { model | pageState = Loaded (Commands newModel) } => Cmd.map CommandsMsg cmd

            ( GuildMsg subMsg, Guild id subModel ) ->
                let
                    ( newModel, cmd ) =
                        Guild.update subMsg subModel
                in
                    { model | pageState = Loaded (Guild id newModel) } => Cmd.map GuildMsg cmd

            ( Receive str, page ) ->
                let
                    newPage =
                        case page of
                            Guilds subModel ->
                                Guilds (Guilds.init str)

                            Guild id subModel ->
                                Guild id (Guild.init str id)

                            Commands subModel ->
                                Commands (Commands.init str)

                            _ ->
                                let
                                    _ =
                                        Debug.log "page" page
                                in
                                    page
                in
                    { model | value = str, pageState = Loaded newPage } => Cmd.none

            ( _, NotFound ) ->
                model => Cmd.none

            ( _, _ ) ->
                model => Cmd.none


main : Program Flags Model Msg
main =
    Navigation.programWithFlags (Route.fromLocation >> SetRoute)
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
