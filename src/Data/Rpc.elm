module Data.Rpc exposing (..)

import Json.Encode exposing (object, encode, string, list)


-- MODEL --


type alias Rpc =
    { command : String
    , args : List String
    }



-- HELPERS --


buildRpc : String -> List String -> Rpc
buildRpc name args =
    Rpc name args


encodeRpc : Rpc -> Json.Encode.Value
encodeRpc rpc =
    object
        [ ( "command", string rpc.command )
        , ( "args", list (List.map string rpc.args) )
        ]
