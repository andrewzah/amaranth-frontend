module Data.Error exposing (Error, errorDecoder, errorListDecoder)

{-| Conform to JSON spec by having errors be
separate from valid responses. |
-}

import Json.Decode as Decode exposing (Decoder, list, string)
import Json.Decode.Pipeline exposing (decode, required)


type alias Error =
    { key : String
    , value : String
    }



-- SERIALIZATION --


errorDecoder : Decoder Error
errorDecoder =
    decode Error
        |> required "key" string
        |> required "value" string


errorListDecoder : Decoder (List Error)
errorListDecoder =
    Decode.list errorDecoder
