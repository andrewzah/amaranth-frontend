module Data.Commands
    exposing
        ( Commands
        , Command
        , wsString
        , wsDecoder
        , CommandsWebSocketResponse(..)
        )

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (custom, decode, hardcoded, required)
import Json.Encode exposing (encode)
import Data.Rpc exposing (buildRpc, encodeRpc)
import Data.Error exposing (Error, errorListDecoder)


-- MODEL --


type alias Command =
    { name : String
    , namespace : String
    , description : String
    , permissions : Int
    , minArgs : Int
    , maxArgs : Int
    }


type alias Commands =
    List Command



-- COMMANDS RESPONSE --


type CommandsWebSocketResponse
    = ValidCommandsResponse Commands
    | ErrorCommandsResponse (List Error)


type alias CommandsValidResponse =
    { data : Commands }


type alias CommandsErrorResponse =
    { errors : List Error }



-- SERIALIZATION --


decoder : Decoder Commands
decoder =
    Decode.list commandDecoder


commandDecoder : Decoder Command
commandDecoder =
    decode Command
        |> required "name" Decode.string
        |> required "namespace" Decode.string
        |> required "description" Decode.string
        |> required "permissions" Decode.int
        |> required "min_args" Decode.int
        |> required "max_args" Decode.int


commandsResponseDecoder : Decoder CommandsValidResponse
commandsResponseDecoder =
    decode CommandsValidResponse
        |> required "data" decoder


commandsErrorResponseDecoder : Decoder CommandsErrorResponse
commandsErrorResponseDecoder =
    decode CommandsErrorResponse
        |> required "errors" errorListDecoder


validCommandsDecoder : Decoder CommandsWebSocketResponse
validCommandsDecoder =
    Decode.map (\response -> ValidCommandsResponse response.data)
        commandsResponseDecoder


errorCommandsDecoder : Decoder CommandsWebSocketResponse
errorCommandsDecoder =
    Decode.map (\response -> ErrorCommandsResponse response.errors)
        commandsErrorResponseDecoder


wsDecoder : Decoder CommandsWebSocketResponse
wsDecoder =
    Decode.oneOf
        [ validCommandsDecoder
        , errorCommandsDecoder
        ]



-- WEBSOCKET HELPERS --


wsString : String
wsString =
    let
        rpc =
            buildRpc "commands" []
                |> encodeRpc
    in
        encode 0 rpc
