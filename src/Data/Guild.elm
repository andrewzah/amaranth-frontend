module Data.Guild
    exposing
        ( Guild
        , ID
        , decoder
        , idParser
        , idToString
        , wsString
        , wsDecoder
        , GuildWebSocketResponse(..)
        , iconUrl
        )

import Data.Guild.Role as Role exposing (Role)
import Data.Guild.Emoji as Emoji exposing (Emoji)
import Json.Decode as Decode exposing (Decoder, decodeValue, succeed)
import Json.Decode.Pipeline exposing (optional, decode, hardcoded, required)
import UrlParser
import Data.Rpc as Rpc exposing (buildRpc, encodeRpc)
import Data.Error exposing (Error, errorListDecoder)
import Json.Encode exposing (encode)


-- MODEL --


type ID
    = ID String


type alias Guild =
    { name : String
    , id : String
    , region : String
    , icon : String
    , ownerID : String
    , roles : List Role
    , splash : String
    , emoji : List Emoji
    }



-- GUILD <int> RESPONSE --


type GuildWebSocketResponse
    = ValidGuildResponse Guild
    | ErrorGuildResponse (List Error)


type alias GuildValidResponse =
    { data : Guild }


type alias GuildErrorResponse =
    { errors : List Error }



-- SERIALIZATION --


decoder : Decoder Guild
decoder =
    decode Guild
        |> required "name" Decode.string
        |> required "id" Decode.string
        |> required "region" Decode.string
        |> required "icon" Decode.string
        |> required "owner_id" Decode.string
        |> required "roles" (Decode.list Role.decoder)
        |> optional "splash" Decode.string "none"
        |> optional "emoji" (Decode.list Emoji.decoder) []


guildResponseDecoder : Decoder GuildValidResponse
guildResponseDecoder =
    decode GuildValidResponse
        |> required "data" decoder


guildErrorResponseDecoder : Decoder GuildErrorResponse
guildErrorResponseDecoder =
    decode GuildErrorResponse
        |> required "errors" errorListDecoder


validGuildDecoder : Decoder GuildWebSocketResponse
validGuildDecoder =
    Decode.map (\response -> ValidGuildResponse response.data)
        guildResponseDecoder


errorGuildDecoder : Decoder GuildWebSocketResponse
errorGuildDecoder =
    Decode.map (\response -> ErrorGuildResponse response.errors)
        guildErrorResponseDecoder


wsDecoder : Decoder GuildWebSocketResponse
wsDecoder =
    Decode.oneOf
        [ validGuildDecoder
        , errorGuildDecoder
        ]



-- WEBSOCKET HELPERS --


wsString : String -> String
wsString id =
    let
        rpc =
            buildRpc "guild" [ id ]
                |> encodeRpc
    in
        encode 0 rpc



-- HELPERS --


idParser : UrlParser.Parser (ID -> a) a
idParser =
    UrlParser.custom "ID" (Ok << ID)


idToString : ID -> String
idToString (ID id) =
    id


iconUrl : String -> String -> String
iconUrl id hash =
    String.join "/" [ "https://cdn.discordapp.com", "icons", id, (hash ++ ".png" ++ "?size=128") ]
