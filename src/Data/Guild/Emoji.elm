module Data.Guild.Emoji exposing (Emoji, decoder)

import Data.Guild.Role as Role exposing (Role)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (custom, decode, required)


-- MODEL --


type alias Emoji =
    { id : Int
    , name : String
    , roles : List Role
    , require_colons : Bool
    , managed : Bool
    }



-- SERIALIZATION --


decoder : Decoder Emoji
decoder =
    decode Emoji
        |> required "id" Decode.int
        |> required "name" Decode.string
        |> required "roles" (Decode.list Role.decoder)
        |> required "require_colons" Decode.bool
        |> required "managed" Decode.bool
