module Data.Guild.Role exposing (Role, decoder, hexColor)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (custom, decode, required)
import Hex


-- MODEL --


type alias Role =
    { color : Int
    , name : String
    , hoist : Bool
    , mentionable : Bool
    , managed : Bool
    , id : String
    , position : Int
    , permissions : Int
    }



-- SERIALIZATION --


decoder : Decoder Role
decoder =
    decode Role
        |> required "color" Decode.int
        |> required "name" Decode.string
        |> required "hoist" Decode.bool
        |> required "mentionable" Decode.bool
        |> required "managed" Decode.bool
        |> required "id" Decode.string
        |> required "position" Decode.int
        |> required "permissions" Decode.int



-- HELPERS --


hexColor : Int -> String
hexColor val =
    "#" ++ Hex.toString val
