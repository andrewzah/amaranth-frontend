module Data.Guilds
    exposing
        ( Guilds
        , wsString
        , wsDecoder
        , GuildsWebSocketResponse(..)
        )

import Data.Guild as Guild exposing (Guild)
import Json.Decode as Decode exposing (Decoder, decodeValue, succeed)
import Json.Decode.Pipeline exposing (optional, decode, hardcoded, required)
import Data.Rpc as Rpc exposing (buildRpc, encodeRpc)
import Data.Error exposing (Error, errorListDecoder)
import Json.Encode exposing (encode)


-- MODEL --


type alias Guilds =
    List Guild



-- GUILDS RESPONSE --


type GuildsWebSocketResponse
    = ValidGuildsResponse Guilds
    | ErrorGuildsResponse (List Error)


type alias GuildsValidResponse =
    { data : Guilds }


type alias GuildsErrorResponse =
    { errors : List Error }



-- SERIALIZATION --


decoder : Decoder Guilds
decoder =
    Decode.list Guild.decoder


guildsResponseDecoder : Decoder GuildsValidResponse
guildsResponseDecoder =
    decode GuildsValidResponse
        |> required "data" decoder


guildsErrorResponseDecoder : Decoder GuildsErrorResponse
guildsErrorResponseDecoder =
    decode GuildsErrorResponse
        |> required "errors" errorListDecoder


validGuildsDecoder : Decoder GuildsWebSocketResponse
validGuildsDecoder =
    Decode.map (\response -> ValidGuildsResponse response.data)
        guildsResponseDecoder


errorGuildsDecoder : Decoder GuildsWebSocketResponse
errorGuildsDecoder =
    Decode.map (\response -> ErrorGuildsResponse response.errors)
        guildsErrorResponseDecoder


wsDecoder : Decoder GuildsWebSocketResponse
wsDecoder =
    Decode.oneOf
        [ validGuildsDecoder
        , errorGuildsDecoder
        ]



-- WEBSOCKET HELPERS --


wsString : String
wsString =
    let
        rpc =
            buildRpc "guilds" []
                |> encodeRpc
    in
        encode 0 rpc
