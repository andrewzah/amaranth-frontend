module Views.Error exposing (formatError, formatErrors)

{-| Reusable view for displaying errors. |
-}

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Data.Error exposing (Error)


formatErrors : List Error -> Html msg
formatErrors errors =
    div [ class "errors" ] (List.map formatError errors)


formatError : Error -> Html msg
formatError error =
    String.join ": " [ error.key, error.value ]
        |> text
