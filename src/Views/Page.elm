module Views.Page exposing (ActivePage(..), frame)

{-| The frame around a typical page - that is, the header and footer.
-}

import Html exposing (Html, Attribute, li, i, text, ul, a, p, label, input, nav, h1, div, header)
import Html.Attributes as Attribute exposing (for, type_, classList, id, class, href)
import Route exposing (Route)
import Html.Events exposing (Options, onWithOptions, onClick)


{-| Determines which navbar link (if any) will be rendered as active.
Note that we don't enumerate every page here, because the navbar doesn't
have links for every page. Anything that's not part of the navbar falls
under Other.
-}
type ActivePage
    = Other
    | Home
    | Commands
    | Guilds
    | About


frame : Bool -> ActivePage -> Html msg -> Html msg
frame isLoading page content =
    div
        [ class "page-wrapper" ]
        [ viewHeader isLoading page
        , div [ classList [ ( "loading", isLoading ) ] ] [ content ]
        ]


viewHeader : Bool -> ActivePage -> Html msg
viewHeader isLoading page =
    let
        brandID =
            case page of
                Home ->
                    "brand-home"

                _ ->
                    "brand-home"
    in
        header
            [ id "navbar" ]
            [ h1 [ id brandID ] [ a [ Route.href Route.Home ] [ text "Amaranth" ] ]
            , div []
                [ ul
                    [ classList
                        [ ( "nav-links-loading", isLoading )
                        , ( "nav-links", (not isLoading) )
                        ]
                    ]
                    [ headerLink page Route.Home "Home"
                    , headerLink page Route.Commands "Commands"
                    , headerLink page Route.Guilds "Guilds"
                    , headerLink page Route.About "About"
                    , heroLink
                    ]
                ]
            ]


headerLink : ActivePage -> Route -> String -> Html msg
headerLink page route msg =
    li [ classList [ ( "link", True ), ( "active", (isActive page route) ) ] ]
        [ a [ Route.href route ] [ text msg ] ]


linkAttrs : msg -> String -> List (Attribute msg)
linkAttrs message link =
    [ onClick message
    , href link
    , class "nav-link"
    ]


heroLink : Html msg
heroLink =
    li [ class "link", class "hero-link" ]
        [ a [ href "https://discordapp.com/api/oauth2/authorize?client_id=420338377737633802&permissions=2146958583&scope=bot" ]
            [ text "Invite " ]
        ]


isActive : ActivePage -> Route -> Bool
isActive page route =
    case ( page, route ) of
        ( Home, Route.Home ) ->
            True

        ( Commands, Route.Commands ) ->
            True

        ( Guilds, Route.Guilds ) ->
            True

        ( About, Route.About ) ->
            True

        _ ->
            False
