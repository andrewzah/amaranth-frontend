module Request.Helpers exposing (buildApiUrl, rpcUrl)


buildApiUrl : { model | apiUrl : String } -> String -> String
buildApiUrl model str =
    model.apiUrl ++ str


rpcUrl : { model | apiUrl : String } -> String
rpcUrl model =
    buildApiUrl model "rpc"
