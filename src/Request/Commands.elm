module Request.Commands exposing (get)

import Data.Commands as Commands exposing (Commands, Command)
import Http
import HttpBuilder exposing (RequestBuilder, withExpect, withQueryParams)
import Json.Decode as Decode
import Request.Helpers exposing (apiUrl)


-- GET --


get : Http.Request Commands
get =
    apiUrl ("/commands")
        |> HttpBuilder.get
        |> HttpBuilder.withExpect (Http.expectJson (Decode.field "commands" Commands.decoder))
        |> HttpBuilder.toRequest
