module Request.WebSocket exposing (listen)

import WebSocket
import Request.Helpers exposing (baseApiUrl)


type Msg
    = Guild String
    | Guilds
    | Commands
    | NewRpc String


listen model =
    WebSocket.listen baseApiUrl NewRpc
