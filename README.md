# Amaranth Frontend

This is a minimal Elm frontend for data from [Amaranth](https://github.com/azah/amaranth).

It uses websockets to send RPCs and receive data about Amaranth's current commands, guilds, statistics, etc.

## Contributing

Anyone is welcome to contribute, either by submitting code or just letting Andrew know about any issues.

For pull requests, please commit feature changes on a separate branch prefixed with `feature/`.

1. Fork it ( https://github.com/azah/amaranth-frontend/fork )
2. Create your feature branch (git checkout -b feature/my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [azah](https://github.com/azah) Andrew Zah - creator, maintainer
