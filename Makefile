SCSS_SOURCE     :=  scss/main.scss
CSS_TARGET      :=  dist/main.css

ELM_SOURCE      :=  src/Main.elm
JS_TARGET       :=  dist/elm.js
PROD_JS_TARGET  :=  dist/elm.js
DEV_JS_TARGET   :=  dist/elm.js

HTML_SOURCE     := src/index.html
HTML_TARGET     := dist/index.html

.PHONY: env dev

env:
	bin/env.sh > dist/env.js

dev:
	elm-make --warn $(ELM_SOURCE) --output $(DEV_JS_TARGET) --debug

release:
	elm-make $(ELM_SOURCE) --output $(JS_TARGET)

nowarn:
	elm-make $(ELM_SOURCE) --debug --output $(DEV_JS_TARGET)

minify: $(JS_TARGET)
	minify --output dist/elm.js $(JS_TARGET)

minifyd: $(DEV_JS_TARGET)
	minify --output dist/elm.js $(DEV_JS_TARGET)

scss:
	sass $(SCSS_FILE) $(CSS_TARGET)

dist: env release minify scss
	cp $(HTML_SOURCE) $(HTML_TARGET)

docker: dist
	docker-compose build
	docker-compose down
	docker-compose up

deploy: dist
	docker-compose build
	docker push andrewzah/amaranth-frontend
